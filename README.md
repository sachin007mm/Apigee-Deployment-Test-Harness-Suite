# Apigee Deployment Test Harness Suite

Apigee Automated Deployment Suite over node.js application will enable customers to adopt for continuous deployment.

# Solution Approach

Develop ready to deploy solution for any customers using Apigee Cloud offering. ​​

Easy Integration of existing SCM with Apigee instance management APIs over OAuth 2.0.  ​

Enabling customers to create CICD structure within API Lifecycle. ​

```Develop --> Publish (Mock) --> Test Deploy(dev) --> Integration/Security Test (DAST)``` 

```Analysis & Reporting --> Deploy & Test (QA) --> Deploy & Test(Prod)```

OWASP based security testing suite using ZAP open libraries, to allow active and passive API scan capabilities. ​

Testing Analysis and Reporting at each stage before business checkout ​

Integration suite can also include stages to regression testing as a pre-requisite before deployment of any changes.

 ![Solution Design](/Artifacts/solution.png)