var reporter = require('cucumber-html-reporter');

var options = {
    theme: 'bootstrap',
    jsonFile: 'test/proxy-report.json',
    output: 'test/cucumber-report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    metadata: {
        "App version": "0.0.1",
        "Test Environment": "test",
        "Executed": "Remote"
    }
};

reporter.generate(options);