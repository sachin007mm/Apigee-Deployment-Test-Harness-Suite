Feature:
    Cognizant Apigee proxy test harness scenarios

    Scenario: Setting body payload in Post request
        Given I set body to { "requisitionNumber":44354221,"approverUname": "saint,Jhon P","approvalAction": "APPROVED","note":"need Next approver information" }
        When I POST to /ewddu
        Then response body path $.response.requisitionNumber should be 44354221
        * response body path $.response.nextApprover should be ganesh.meghale@summne.com
        * response body path $.response.systemDate should be 06-DEC-2018