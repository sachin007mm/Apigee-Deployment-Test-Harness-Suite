'use strict';

const ResponseGherkin = require('./commonsteps');
const cucumber = require('cucumber');
const apickli = require('apickli');
const apigeeHost = 'maheshdhummi-eval-test.apigee.net';
const apigeeScheme = 'http';

ResponseGherkin(cucumber);

const {Before,setDefaultTimeout} = cucumber;

setDefaultTimeout(15 * 1000);

Before(function() {
    this.apickli = new apickli.Apickli(apigeeScheme, apigeeHost);
    this.apickli.addRequestHeader('Cache-Control', 'no-cache');
});

module.exports = require('apickli/apickli-gherkin');