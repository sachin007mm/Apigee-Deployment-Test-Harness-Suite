'use strict';

module.exports = ({Then}) => {

    Then(/^I'm checking the responses$/,function(){
        //do nothing
    });

    Then(/^responses body path (.*) should exists$/, function (path,callback) {
        const assertion = this.apickli.evaluatePathInResponseBody(path);
        callback(assertion ? null : `could not find ${path} in response body`);
    });

    Then(/^responses body at path (.*) equals (((?!of type).*))$/, function (path, value, callback) {
        const assertion = this.apickli.assertPathInResponseBodyMatchesExpression(path, value);
        callback(assertion && assertion.success ? null : `could not find ${path} in response body with value ${value}`);
    });
};